<?php
return [
	// this token can be found in toggl profile
	'toggl_token' => 'your toggle token',
	 // workspace name or keep blank for first
	'toggl_workspace' => null,

	// your traffika credentials
	'traffika_username' => 'you@usertechnologies.com',
	'traffika_password' => 'your password',

	// default activity if none is selected in toggl
	'traffika_default_activity' => 'development + unit test',

	// no need to modify the rest now
	'traffika_company_domain' => 'usertechnologies.com',
	'traffika_api_url' => 'https://appu.gettraffika.com/api'
];
